/**
 * @file 不使用rollup plugin 的场景下，产出代码的兼容性测试
 * @author Franck Chen(chenfan02@baidu.com)
 */

// good case, 无default全量引入
import * as personInfo from '../dep/personInfo';

// good case, 匿名函数测试
import '../dep/anonymous';

// good case, default引入测试
import cf from '../dep/defaultTest';

// good case, 测试重复引用
import '../dep/loop';

// good case, 引入方法
import cfFunc from '../dep/defaultFunction';

// bad case, 包含import的全量引入
import * as chenfan from '../dep/defaultTest';

console.log(personInfo.firstName);
console.log(cf.age);
console.log(chenfan);

// good case 引入对象
console.log(personInfo.commonInfo);

// good case 引入方式
console.log(personInfo.getName());

cfFunc();

var firstName = 'Michael';
var lastName = 'Jackson';
var year = 1958;
var commonInfo = {
    age: 29,
    hobby: 'football'
};

var getName = function () {
    return firstName + lastName;
};

export {firstName, lastName, year, commonInfo, getName};

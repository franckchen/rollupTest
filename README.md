rollup tree shaking和兼容性测试

执行指令  
yarn  
rollup -c config/exportTestConfig.js  
rollup -c config/importTestConfig.js  

good case代表转换过程中不会产生es5代码，完全兼容ie6  
bad case代表转换过程中会产生es5代码，但是使用了官方的es3-plugin后，生成的代码已优化为es3版本  

最终生成的代码，复合amd规范  

import es3 from 'rollup-plugin-es3';

export default {
    entry: 'src/importTest.js',
    format: 'amd',
    plugins: [
        es3(['defineProperty', 'freeze'])
    ],
    dest: 'asset/importTest.js'
};

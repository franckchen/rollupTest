import es3 from 'rollup-plugin-es3';

export default {
    entry: 'src/exportTest.js',
    format: 'amd',
    plugins: [
        es3(['defineProperty', 'freeze'])
    ],
    dest: 'asset/exportTest.js'
};

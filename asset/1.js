var firstName = 'Michael';
var lastName = 'Jackson';
var year = 1958;
var commonInfo = {
    age: 29,
    hobby: 'football'
};

var getName = function () {
    return firstName + lastName;
};

(function () {
    console.log('anonymous function');
})();

var cf = {
    name: 'chenfan',
    age: 29
};

var cf$1 = 'chenfan';

var age = 29;


var chenfan = Object.freeze({
	default: cf,
	cf: cf$1,
	age: age
});

console.log('hello');

console.log(year);

var text = 'hello world';

var cfFunc = function () {
    console.log(text);
};

// good case
// 匿名函数测试 good case
// good case default引入测试
// bad case
// good case 测试重复引用
console.log(firstName);
console.log(cf.age);
console.log(chenfan);

// good case 引入对象
console.log(commonInfo);

// good case 引入方式
console.log(getName());

cfFunc();
